# Full-stack IoT solution that interfaces to real-world sensors

Development of a full-stack IoT solution that interfaces to real-world sensors. Which includes an MQTT framework architecture, an MQTT publisher sensor application, a multiple MQTT subscriber actuator app and a Qt MQTT visualization GUI application.